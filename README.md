# dcdp-relay

Collect events from connected devices and relay them to a running [dcdp-server](https//gitlab.com/chris.willing.oss/dcdp-server)


## Description

[DCDP Server](https//gitlab.com/chris.willing.oss/dcdp-server) software exists to marshall events from connected USB devices and distribute these events as DCDP messages to network connected clients. However the number of connected USB devices is ultimately limited by, for example, the number of physical connection points available on the machine running the server software.

A _dcdp-server_ is already able to deal with so called _device clients_. These are envisaged as virtual devices i.e. software generated devices which connect to a _dcdp-server_ where they are treated as if they are real physically attached devices. This _dcdp-relay_ software is similar to a _dcdp-server_ in that it collects events from physically connected devices. However instead of distributing event messages to all network connected clients, it relays the events as _device client_ messages to a particular _dcdp-server_. 

Since the _dcdp-relay_ software could run on cheap hardware (a Rapberry Pi could accommodate 4 USB devices), several of them could be used to send their device events to a _dcdp-server_. The number of devices potentially available to a _dcdp-server_ therefore becomes almost unlimited.


## Contributing
Contributions and bug fixes are welcome as [Merge Requests](https://gitlab.com/chris.willing.oss/dcdp-relay/-/merge_requests); comments and suggestions as [issues](https://gitlab.com/chris.willing.oss/dcdp-relay/-/issues). 


## License
MIT
